// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const fs = require('fs')
const path = require('path')
const assert = require('assert')
const {parseRequest, verifySignature, sign} = require('http-signature')
const fetch = require('node-fetch')
const {URL} = require('url')

const _ = require('lodash')
const debug = require('debug')('activitypub-mock:index')
const as2 = require('activitystrea.ms')
const uuid = require('uuid')
const keypair = require('keypair')

const {name, version} = JSON.parse(fs.readFileSync(path.join(__dirname, 'package.json'), {encoding: 'utf8'}))

const JSONLD_TYPE = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
const AS2_TYPE = 'application/activity+json'
const JSON_TYPE = 'application/json; charset=utf-8'

const AS2 = 'https://www.w3.org/ns/activitystreams'
const SEC = 'https://w3id.org/security/v1'

const PUBLIC = 'https://www.w3.org/ns/activitystreams#Public'

const spareKeys = require('./spare-keys')

const APROPS = ['to', 'cc', 'bto', 'bcc']

class ActivityPubMock {
  constructor (props) {
    // Save initialization properties for later
    this.props = _.cloneDeep(props)
    this.port = props.port || 8080
    this.address = props.address || 'localhost'
    this.urlRoot = props.urlRoot || `http://${this.address}:${this.port}`
    this.keys = {}
    if (props.oauth2) {
      this.keys = _.invert(props.oauth2)
    }
    this.users = new Map()
    this.inboxes = new Map()
    this.outboxes = new Map()
    this.followers = new Map()
    this.following = new Map()
    this.liked = new Map()
    this.activities = new Map()
    this.rsa = new Map()
    this.content = new Map()
    this.deletedContent = new Map()
  }

  url (rel) {
    return `${this.urlRoot}${rel}`
  }

  mintID () {
    return uuid.v4()
  }

  async handler (req, res) {
    debug(`handler arguments length: ${arguments.length}`)
    const getPrincipal = () => {
      const {authorization} = req.headers
      if (!authorization) {
        return null
      } else {
        const match = authorization.match(/^Bearer (.*)$/)
        if (!match) {
          return null
        } else {
          const [, key] = match
          return this.keys[key]
        }
      }
    }

    const json = () => {
      return new Promise((resolve, reject) => {
        let buf = ''
        req.on('error', (err) => {
          reject(err)
        })
        req.on('data', (chunk) => {
          buf = `${buf}${chunk}`
        })
        req.on('end', () => {
          let obj = null
          try {
            obj = JSON.parse(buf)
          } catch (err) {
            return reject(err)
          }
          return resolve(obj)
        })
      })
    }

    const principal = getPrincipal()

    const as2out = (obj, code = 200, headers = {}) => {
      const {accept} = req.headers
      debug(accept)
      if (!accept || accept.match(/application\/ld\+json/) || accept.match(/\*\/\*/)) {
        res.writeHead(code, http.STATUS_CODES[code], _.extend(headers, {'Content-Type': JSONLD_TYPE}))
      } else if (accept.match(/application\/activity\+json/)) {
        res.writeHead(code, http.STATUS_CODES[code], _.extend(headers, {'Content-Type': AS2_TYPE}))
      } else if (accept.match(/application\/json/)) {
        res.writeHead(code, http.STATUS_CODES[code], _.extend(headers, {'Content-Type': JSON_TYPE}))
      }
      debug(obj.id)
      debug('obj.prettyWrite()')
      obj.prettyWrite((err, doc) => {
        debug('Finished prettyWrite')
        if (err) {
          debug(`Got an error outputting obj: ${err.message}`)
          servererror(err.message)
        } else {
          debug(`No error outputting obj; ${doc.length} chars`)
          res.end(doc)
        }
      })
    }

    const servererror = (message) => {
      // If we get here, no match
      res.writeHead('500', 'Server error', {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'error', message: message}))
    }

    const notfound = (message) => {
      // If we get here, no match
      res.writeHead('404', 'Not found', {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'error', message: message}))
    }

    const unauthorized = (message) => {
      // If we get here, no match
      res.writeHead('403', 'Unauthorized', {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'error', message: message}))
    }

    const methodnotallowed = (message) => {
      // If we get here, no match
      res.writeHead('405', 'Method Not Allowed', {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'error', message: `${req.method} not allowed on ${req.url}`}))
    }

    const accepted = (message) => {
      // If we get here, no match
      res.writeHead('202', http.STATUS_CODES[202], {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'OK', message: message}))
    }

    const gone = (message) => {
      // If we get here, no match
      res.writeHead('410', http.STATUS_CODES[410], {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify({status: 'error', message: message}))
    }

    const idMatch = (value, id) => {
      debug(`idMatch(${value}, ${id})`)
      if (_.isString(value)) {
        debug('value is a string; using equals')
        return value === id
      } else if (_.isArray(value)) {
        debug('value is an array; using _.some()')
        return _.some(value, (item) => { return idMatch(item, id) })
      } else if (_.isObject(value)) {
        debug('value is an object; using equals with value.id')
        debug(value)
        debug(value.first)
        return value.first.id === id
      } else {
        debug('unrecognized value type; must not be a match')
        return false
      }
    }

    const authorOrAddressee = (activity, nickname) => {
      debug(`Checking if ${nickname} is allowed to see ${activity.id}`)
      if (nickname) {
        debug('Checking public and private addressing')
        const id = this.url(`/user/${nickname}`)
        const props = ['actor', 'to', 'cc', 'bto', 'bcc']
        for (const prop of props) {
          debug(`Checking against ${prop}`)
          const value = activity.get(prop)
          debug(`${prop} value is ${value}`)
          if (idMatch(value, id) || idMatch(value, PUBLIC)) {
            return true
          }
        }
      } else {
        debug('Checking only public addressing')
        const props = ['to', 'cc', 'bto', 'bcc']
        for (const prop of props) {
          debug(`Checking against ${prop}`)
          const value = activity.get(prop)
          debug(`${prop} value is ${value}`)
          if (idMatch(value, PUBLIC)) {
            return true
          }
        }
      }
      return false
    }

    const routes = {
      '^/version$': {
        GET: (match, req, res) => {
          debug('matched version')
          res.writeHead('200', 'OK', {'Content-Type': 'application/json; charset=utf-8'})
          res.end(JSON.stringify({name: name, version: version}))
        }
      },
      '^/user/([a-z0-9]+)$': {
        GET: (match, req, res) => {
          debug('matched user route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.users.has(nickname)) {
            as2out(this.users.get(nickname))
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/user/([a-z0-9]+)/inbox$': {
        GET: (match, req, res) => {
          debug('matched user inbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (nickname !== principal) {
            unauthorized(`Only user ${nickname} can read their own inbox`)
          } else if (this.inboxes.has(nickname)) {
            const activities = this.inboxes.get(nickname)
            debug(`Got ${activities.length} activities for ${nickname} inbox`)
            const inbox = as2.collection()
              .id(this.url(`/user/${nickname}/inbox`))
              .summary(`${nickname} inbox`)
              .totalItems(activities.length)
              .first(
                as2.collectionPage()
                  .id(this.url(`/user/${nickname}/inbox/page/1`))
                  .summary(`page 1 of ${nickname} inbox`)
                  .items(activities)
                  .get())
              .get()
            debug(inbox)
            as2out(inbox)
          } else if (this.users.has(nickname)) {
            // No inbox, but user is defined; return an empty inbox
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/inbox`))
              .summary(`${nickname} inbox`)
              .totalItems(0)
              .publishedNow()
              .get())
          } else {
            notfound(`no such user: ${nickname}`)
          }
        },
        POST: async (match, req, res) => {
          debug('matched POST user inbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          const parsed = parseRequest(req)
          debug(parsed)
          const {keyId} = parsed.params
          debug(`getting public key for ${keyId}`)
          let pub = null
          try {
            pub = await this.getPublicKey(keyId)
          } catch (err) {
            return unauthorized(err.message)
          }
          debug(`verifying signature for ${keyId}`)
          if (!verifySignature(parsed, pub)) {
            return unauthorized(`Incorrect signature for key ${keyId}`)
          }
          const body = await json()
          debug(body)
          const activity = await this.as2ify(body)
          debug(activity)
          if (!keyId === activity.actor.first.id) {
            return unauthorized(`Signature for ${keyId} but actor is ${activity.actor.first.id}`)
          }
          accepted(`${activity.id} accepted for processing`)
          if (!this.inboxes.has(nickname)) {
            this.inboxes.set(nickname, [activity])
          } else {
            this.inboxes.get(nickname).unshift(activity)
          }
          this.applyRemote(activity)
        }
      },
      '^/user/([a-z0-9]+)/inbox/1$': {
        GET: (match, req, res) => {
          debug('matched user inbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (nickname !== principal) {
            unauthorized(`Only user ${nickname} can read their own inbox`)
          } else if (this.inboxes.has(nickname)) {
            const activities = this.inboxes.get(nickname)
            debug(`Got ${activities.length} activities for ${nickname} inbox`)
            if (activities.length === 0) {
              notfound(`no activities in ${nickname} inbox`)
            } else {
              const page = as2.collectionPage()
                .id(this.url(`/user/${nickname}/inbox/page/1`))
                .summary(`page 1 of ${nickname} inbox`)
                .items(activities)
                .get()
              as2out(page)
            }
          } else if (this.users.has(nickname)) {
            notfound(`no activities in ${nickname} inbox`)
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/user/([a-z0-9]+)/outbox$': {
        GET: (match, req, res) => {
          debug('matched user outbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.outboxes.has(nickname)) {
            const activities = this.outboxes.get(nickname)
            // FIXME: filter by user
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/outbox`))
              .summary(`${nickname} outbox`)
              .totalItems(activities.length)
              .first(
                as2.collectionPage()
                  .id(this.url(`/user/${nickname}/outbox/page/1`))
                  .summary(`page 1 of ${nickname} outbox`)
                  .items(activities)
                  .get())
              .get())
          } else if (this.users.has(nickname)) {
            // No inbox, but user is defined; return an empty inbox
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/outbox`))
              .summary(`${nickname} outbox`)
              .totalItems(0)
              .publishedNow()
              .get())
          } else {
            notfound(`no such user: ${nickname}`)
          }
        },
        POST: async (match, req, res) => {
          debug('matched POST user outbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (nickname !== principal) {
            unauthorized(`Only user ${nickname} can post to their outbox`)
          } else {
            const body = await json()
            debug(body)
            const id = this.mintID()
            debug(id)
            body.id = this.url(`/activity/${id}`)
            body.actor = this.url(`/user/${nickname}`)
            debug(body)
            await this.apply(body)
            const activity = await this.as2ify(body)
            debug(activity)
            this.activities.set(id, activity)
            if (!this.outboxes.has(nickname)) {
              this.outboxes.set(nickname, [activity])
            } else {
              this.outboxes.get(nickname).unshift(activity)
            }
            as2out(activity, 201, {Location: body.id})
            this.distribute(activity, nickname)
          }
        }
      },
      '^/user/([a-z0-9]+)/outbox/1$': {
        GET: (match, req, res) => {
          debug('matched user outbox route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.outboxes.has(nickname)) {
            const activities = this.outboxes.get(nickname)
            if (activities.length === 0) {
              notfound(`no activities for user: ${nickname}`)
            } else {
              const page = as2.collectionPage()
                .id(this.url(`/user/${nickname}/outbox/page/1`))
                .summary(`page 1 of ${nickname} outbox`)
                .items(activities)
                .get()
              as2out(page)
            }
          } else if (this.users.has(nickname)) {
            notfound(`no activities for user: ${nickname}`)
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/user/([a-z0-9]+)/followers$': {
        GET: (match, req, res) => {
          debug('matched user followers route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.followers.has(nickname)) {
            const followers = this.followers.get(nickname)
            // FIXME: filter by user
            const collection = as2.collection()
              .id(this.url(`/user/${nickname}/followers`))
              .summary(`${nickname} followers`)
              .totalItems(followers.length)
              .first(
                as2.collectionPage()
                  .id(this.url(`/user/${nickname}/followers/page/1`))
                  .summary(`page 1 of ${nickname} followers`)
                  .items(followers)
                  .get())
              .get()
            as2out(collection)
          } else if (this.users.has(nickname)) {
            // No inbox, but user is defined; return an empty inbox
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/followers`))
              .summary(`${nickname} followers`)
              .totalItems(0)
              .publishedNow()
              .get())
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/user/([a-z0-9]+)/following$': {
        GET: (match, req, res) => {
          debug('matched user following route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.following.has(nickname)) {
            // FIXME: filter by user
            const following = this.following.get(nickname)
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/following`))
              .summary(`${nickname} following`)
              .totalItems(following.length)
              .first(
                as2.collectionPage()
                  .id(this.url(`/user/${nickname}/following/page/1`))
                  .summary(`page 1 of ${nickname} following`)
                  .items(following)
                  .get())
              .get())
          } else if (this.users.has(nickname)) {
            // No inbox, but user is defined; return an empty inbox
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/following`))
              .summary(`${nickname} following`)
              .totalItems(0)
              .publishedNow()
              .get())
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/user/([a-z0-9]+)/liked$': {
        GET: (match, req, res) => {
          debug('matched user liked route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.liked.has(nickname)) {
            // FIXME: filter by user
            as2out(this.liked.get(nickname))
          } else if (this.users.has(nickname)) {
            // No liked, but user is defined; return an empty collection
            as2out(as2.collection()
              .id(this.url(`/user/${nickname}/liked`))
              .summary(`Things ${nickname} liked`)
              .totalItems(0)
              .publishedNow()
              .get())
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/activity/([a-f0-9]{8}-?[a-f0-9]{4}-?[1-5][a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12})$': {
        GET: (match, req, res) => {
          debug('matched activity route')
          const [, id] = match
          debug(id)
          if (this.activities.has(id)) {
            const activity = this.activities.get(id)
            debug(activity)
            if (authorOrAddressee(activity, principal)) {
              as2out(activity)
            } else {
              unauthorized(`No access to ${activity.id}`)
            }
          } else {
            notfound(`no such activity`)
          }
        }
      },
      '^/user/([a-z0-9]+)/publickey$': {
        GET: (match, req, res) => {
          debug('matched user publickey route')
          const [, nickname] = match
          debug(`got user ${nickname}`)
          if (this.users.has(nickname)) {
            const user = this.users.get(nickname)
            debug(user)
            const pk = user.get(`${SEC}publicKey`)
            debug(pk)
            as2out(pk.first)
          } else {
            notfound(`no such user: ${nickname}`)
          }
        }
      },
      '^/content/([a-f0-9]{8}-?[a-f0-9]{4}-?[1-5][a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12})$': {
        GET: (match, req, res) => {
          debug('matched content route')
          const [, id] = match
          debug(`got id ${id}`)
          if (this.content.has(id)) {
            const content = this.content.get(id)
            as2out(content)
          } else if (this.deletedContent.has(id)) {
            gone(`object deleted`)
          } else {
            notfound(`no such content: ${id}`)
          }
        }
      }
    }
    debug(req.url)
    for (const path in routes) {
      const match = req.url.match(new RegExp(path))
      if (match) {
        const handler = routes[path]
        assert.ok(_.isObject(handler))
        if (handler[req.method]) {
          return handler[req.method](match, req, res)
        } else {
          return methodnotallowed(req, res)
        }
      }
    }
    notfound(`${req.url} not found`)
  }

  async apply (json) {
    debug(`applying activity ${json.id} of type ${json.type}`)
    switch (json.type) {
      case 'https://www.w3.org/ns/activitystreams#Create':
      case 'Create': {
        const id = this.mintID()
        debug(`Creating content with id ${id}`)
        json.object.id = this.url(`/content/${id}`)
        json.object.attributedTo = json.actor
        json.object.published = this.timestamp()
        json.object.updated = json.object.published
        debug(json.object)
        const as2content = await this.as2ify(json.object)
        debug(as2content)
        this.content.set(id, as2content)
        break
      }
      case 'Update': {
        debug(`Updating object with id ${json.object.id}`)
        const uuid = this.contentID(json.object.id)
        debug(`uuid = ${uuid}`)
        if (!this.content.has(uuid)) {
          throw new Error(`No such object: ${json.object.id}`)
        }
        let as2object = this.content.get(uuid)
        debug(as2object)
        const old = await this.jsonify(as2object)
        debug(old)
        const updated = _.extend(old, json.object, {updated: this.timestamp()})
        debug(updated)
        as2object = await this.as2ify(updated)
        debug(as2object)
        this.content.set(uuid, as2object)
        break
      }
      case 'Delete': {
        debug(`Deleting object with id ${json.object.id}`)
        let uuid = null
        if (_.isString(json.object)) {
          uuid = this.contentID(json.object)
        } else if (_.isObject(json.object)) {
          uuid = this.contentID(json.object.id)
        } else {
          throw new Error(`Unexpected object type: ${typeof json.object}`)
        }
        debug(`uuid = ${uuid}`)
        if (!this.content.has(uuid)) {
          throw new Error(`No such object: ${json.object.id}`)
        }
        const as2object = this.content.get(uuid)
        this.content.delete(uuid)
        const tombstone = {
          '@context': {
            '@vocab': AS2
          },
          type: 'Tombstone',
          id: as2object.id,
          published: as2object.published,
          updated: as2object.updated,
          summary: 'Deleted object',
          deleted: this.timestamp(),
          formerType: as2.type
        }
        const as2tombstone = await this.as2ify(tombstone)
        this.deletedContent.set(uuid, as2tombstone)
        break
      }
      case 'Follow': {
        debug('Applying Follow activity')
        const actorNickname = this.nicknameOf(json.actor)
        debug(`actor nickname = ${actorNickname}`)
        const objectId = (_.isObject(json.object)) ? json.object.id : json.object
        debug(`object ID = ${objectId}`)
        if (this.isLocal(objectId)) {
          debug(`object is local`)
          const objectNickname = this.nicknameOf(objectId)
          debug(`object nickname = ${objectNickname}`)
          const actorUser = this.users.get(actorNickname)
          debug(actorUser)
          const objectUser = this.users.get(objectNickname)
          debug(objectUser)
          this.appendInMap(this.following, actorNickname, objectUser)
          this.appendInMap(this.followers, objectNickname, actorUser)
        }
      }
    }
    return json
  }

  appendInMap (map, key, item) {
    const collection = (map.has(key)) ? map.get(key) : []
    collection.push(item)
    map.set(key, collection)
  }

  timestamp () {
    return (new Date()).toISOString()
  }

  async applyRemote (activity) {

  }

  async distribute (activity, nick) {
    debug(`distributing activity ${activity.id} by ${nick}`)
    const addressees = this.collectAddressees(activity)
    debug(`got ${addressees.length} addressees`)
    const expanded = this.expandAddressees(addressees, nick)
    debug(`got ${expanded.length} expanded addressees`)
    Promise.all(expanded.map((addressee) => this.distributeTo(activity, addressee)))
  }

  collectAddressees (activity) {
    const addressees = new Set()
    APROPS.forEach((prop) => {
      debug(`Checking property '${prop}' of activity ${activity.id}`)
      const value = activity.get(prop)
      if (value) {
        const ids = value.toArray().map((obj) => obj.id || obj['@id'])
        debug(`ids from ${prop} of ${activity.id} are ${JSON.stringify(ids)}`)
        ids.forEach((id) => addressees.add(id))
      }
    })
    return Array.from(addressees)
  }

  isLocal (address) {
    return address.substr(0, this.urlRoot.length) === this.urlRoot
  }

  nicknameOf (address) {
    const match = address.match(`^${this.urlRoot}/user/([a-z0-9]+)`)
    return match[1]
  }

  async distributeTo (activity, address) {
    debug(`distributing ${activity.id} to ${address}`)
    if (this.isLocal(address)) {
      debug(`${address} is a local address`)
      const nickname = this.nicknameOf(address)
      debug(`${nickname} is the nickname of ${address}`)
      if (this.inboxes.has(nickname)) {
        debug(`${nickname} already has an inbox; prepending to it`)
        this.inboxes.get(nickname).unshift(activity)
      } else {
        debug(`${nickname} has no inbox; creating it`)
        this.inboxes.set(nickname, [activity])
      }
    } else {
      debug(`${address} is a remote address`)
      const inbox = await this.inboxOf(address)
      debug(`inbox for ${address} is ${inbox}`)
      const actor = activity.actor.first.id
      debug(`actor for ${activity.id} is ${actor}`)
      const nickname = this.nicknameOf(actor)
      debug(`nickname for ${actor} is ${nickname}`)
      const key = this.keyOf(nickname)
      debug(`key for ${nickname} is ${key}`)
      return this.postRemote(activity, inbox, key)
    }
  }

  keyOf (nickname) {
    return this.rsa.get(nickname)
  }

  async inboxOf (address) {
    debug(`Getting inbox for ${address}`)
    const fixed = this.fixupRemote(address)
    debug(`Fixed address is ${fixed}`)
    const res = await fetch(fixed)
    debug(`Status of result is ${res.status}`)
    const body = await res.json()
    debug(body)
    const value = body['ldp:inbox']
    debug(value)
    const inbox = (_.isObject(value)) ? value.id : value
    debug(inbox)
    return inbox
  }

  fixupRemote (url) {
    debug(`Fixing up url ${url}`)
    if (this.props.rootMap) {
      for (const root in this.props.rootMap) {
        debug(`Checking ${url} against ${root}`)
        if (url.substr(0, root.length) === root) {
          debug(`Match between ${url} and ${root}; fixing it up`)
          url = url.replace(root, this.props.rootMap[root])
          debug(`New URL is ${url}`)
          break
        }
      }
    }
    return url
  }

  async postRemote (payload, inbox, key) {
    const fixed = this.fixupRemote(inbox)
    debug(`postRemote mapped ${inbox} to ${fixed}`)
    return new Promise((resolve, reject) => {
      payload.write((err, body) => {
        if (err) {
          reject(err)
        } else {
          const url = new URL(fixed)
          debug(`postRemote has url ${url}`)
          const options = {
            host: url.hostname,
            port: url.port,
            path: `${url.pathname}${url.search}`,
            method: 'POST',
            headers: {
              Accept: JSONLD_TYPE,
              'Content-Type': JSONLD_TYPE,
              'Content-Length': Buffer.byteLength(body, 'utf8')
            }
          }
          debug(options)
          debug(`Starting request for ${url}`)
          const req = http.request(options, (res) => {
            debug(`Finished request for ${url} with status ${res.statusCode}`)
            resolve(res)
          })
          req.on('error', reject)
          debug(`Signing request for ${url}`)
          sign(req, {
            key: key.private,
            keyId: payload.actor.first.id
          })
          debug(`Writing ${body.length} characters for ${url}`)
          req.write(body)
          debug(`Finishing request to ${url}`)
          req.end()
        }
      })
    })
  }

  expandAddressees (addressees, nick) {
    const expanded = new Set()
    const followers = this.url(`/user/${nick}/followers`)
    for (const addressee of addressees) {
      if (addressee === PUBLIC || addressee === followers) {
        if (this.followers.has(nick)) {
          this.followers.get(nick).forEach((obj) => expanded.add(obj.id))
        }
      } else {
        expanded.add(addressee)
      }
    }
    return Array.from(expanded)
  }

  as2ify (obj) {
    return new Promise((resolve, reject) => {
      as2.import(obj, (err, imported) => {
        if (err) {
          reject(err)
        } else {
          resolve(imported)
        }
      })
    })
  }

  jsonify (obj) {
    return new Promise((resolve, reject) => {
      obj.export((err, json) => {
        if (err) {
          reject(err)
        } else {
          resolve(json)
        }
      })
    })
  }

  async start () {
    debug(`Starting server on ${this.address}:${this.port}`)
    this.server = http.createServer(this.handler.bind(this))
    const startNetwork = new Promise((resolve, reject) => {
      const onError = (err) => {
        clearHandlers()
        debug(`Error starting server on ${this.address}:${this.port}: ${err.message}`)
        reject(err)
      }
      const onListening = () => {
        clearHandlers()
        debug(`Successfully started server on ${this.address}:${this.port}`)
        resolve()
      }
      const clearHandlers = () => {
        this.server.removeListener('error', onError)
        this.server.removeListener('listening', onListening)
      }
      this.server.on('error', onError)
      this.server.on('listening', onListening)
      this.server.listen(this.port, this.address)
    })
    assert(_.isFunction(this.importData))
    return Promise.all([startNetwork, this.importData()])
  }

  async stop () {
    debug(`Stopping server on ${this.address}:${this.port}`)
    if (!this.server) {
      throw new Error("Haven't started yet")
    }
    return new Promise((resolve, reject) => {
      const onError = (err) => {
        clearHandlers()
        delete this.server
        debug(`Error stopping server on ${this.address}:${this.port}: ${err.message}`)
        reject(err)
      }
      const onClose = () => {
        clearHandlers()
        delete this.server
        debug(`Successfully stopped server on ${this.address}:${this.port}`)
        resolve()
      }
      const clearHandlers = () => {
        this.server.removeListener('error', onError)
        this.server.removeListener('close', onClose)
      }
      this.server.on('error', onError)
      this.server.on('close', onClose)
      this.server.close()
    })
  }

  async importData () {
    return Promise.all([
      this.importUsers(),
      this.importInboxes(),
      this.importOutboxes(),
      this.importFollowers(),
      this.importFollowing(),
      this.importLiked(),
      this.importContent()
    ])
  }

  async importUsers () {
    debug('importUsers()')
    const promises = []
    if (this.props.users) {
      let i = 0
      for (const nick in this.props.users) {
        const rsa = (this.props.rsa && this.props.rsa[nick]) ? this.props.rsa[nick]
          : (i < spareKeys.length) ? spareKeys[i++]
            : keypair()
        this.rsa.set(nick, rsa)
        const local = {
          '@context': {
            '@vocab': AS2,
            'sec': SEC
          },
          'id': this.url(`/user/${nick}`),
          'type': 'Person',
          'ldp:inbox': this.url(`/user/${nick}/inbox`),
          'as:outbox': this.url(`/user/${nick}/outbox`),
          'as:followers': this.url(`/user/${nick}/followers`),
          'as:following': this.url(`/user/${nick}/following`),
          'as:liked': this.url(`/user/${nick}/liked`),
          'published': (new Date()).toISOString(),
          'sec:publicKey': {
            'id': this.url(`/user/${nick}/publickey`),
            'type': 'sec:Key',
            'summary': `public key for ${nick}`,
            'sec:owner': this.url(`/user/${nick}`),
            'sec:publicKeyPem': rsa.public
          }
        }
        const def = _.extend(local, this.props.users[nick])
        promises.push(new Promise((resolve, reject) => {
          as2.import(def, (err, imported) => {
            if (err) {
              reject(err)
            } else {
              this.users.set(nick, imported)
              resolve()
            }
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importInboxes () {
    const promises = []
    debug('importInboxes()')
    if (_.isObject(this.props.inboxes)) {
      for (const nick in this.props.inboxes) {
        promises.push(new Promise((resolve, reject) => {
          Promise.all(this.props.inboxes[nick].map((activity) => {
            return new Promise((resolve, reject) => {
              as2.import(activity, (err, obj) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(obj)
                }
              })
            })
          })).then(activities => {
            this.inboxes.set(nick, activities)
            resolve()
          }).catch(err => {
            reject(err)
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importOutboxes () {
    debug('importOutboxes()')
    const promises = []
    if (_.isObject(this.props.outboxes)) {
      for (const nick in this.props.outboxes) {
        promises.push(new Promise((resolve, reject) => {
          Promise.all(this.props.outboxes[nick].map((activity) => {
            return new Promise((resolve, reject) => {
              as2.import(activity, (err, obj) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(obj)
                }
              })
            })
          })).then(activities => {
            this.outboxes.set(nick, activities)
            resolve()
          }).catch(err => {
            reject(err)
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importFollowers () {
    debug('importFollowers()')
    const promises = []
    if (_.isObject(this.props.followers)) {
      for (const nick in this.props.followers) {
        promises.push(new Promise((resolve, reject) => {
          Promise.all(this.props.followers[nick].map((follower) => {
            const local = {
              'type': 'Person',
              'summary': 'A person'
            }
            const def = _.extend(local, (_.isString(follower)) ? {id: follower} : follower)
            return new Promise((resolve, reject) => {
              as2.import(def, (err, obj) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(obj)
                }
              })
            })
          })).then(followers => {
            this.followers.set(nick, followers)
            resolve()
          }).catch(err => {
            reject(err)
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importFollowing () {
    debug('importFollowing()')
    const promises = []
    if (_.isObject(this.props.following)) {
      for (const nick in this.props.following) {
        promises.push(new Promise((resolve, reject) => {
          Promise.all(this.props.following[nick].map((following) => {
            const local = {
              'type': 'Person',
              'summary': 'A person'
            }
            const def = _.extend(local, (_.isString(following)) ? {id: following} : following)
            return new Promise((resolve, reject) => {
              as2.import(def, (err, obj) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(obj)
                }
              })
            })
          })).then(following => {
            this.following.set(nick, following)
            resolve()
          }).catch(err => {
            reject(err)
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importLiked () {
    debug('importLiked()')
    const promises = []
    if (_.isObject(this.props.liked)) {
      for (const nick in this.props.liked) {
        promises.push(new Promise((resolve, reject) => {
          Promise.all(this.props.liked[nick].map((liked) => {
            const local = {
              type: 'Object',
              summary: (liked.type) ? `a(n) ${liked.type}` : 'an object'
            }
            const def = _.extend(local, (_.isString(liked)) ? {id: liked} : liked)
            return new Promise((resolve, reject) => {
              as2.import(def, (err, obj) => {
                if (err) {
                  reject(err)
                } else {
                  resolve(obj)
                }
              })
            })
          })).then(liked => {
            const collection = as2.collection()
              .id(this.url(`/user/${nick}/liked`))
              .summary(`Things ${nick} liked`)
              .totalItems(liked.length)
              .first(
                as2.collectionPage()
                  .id(this.url(`/user/${nick}/liked/page/1`))
                  .summary(`page 1 of things ${nick} liked`)
                  .items(liked)
                  .get())
              .get()
            this.liked.set(nick, collection)
            resolve()
          }).catch(err => {
            reject(err)
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  async importContent () {
    debug('importContent()')
    const promises = []
    if (_.isArray(this.props.content)) {
      for (const content of this.props.content) {
        let uuid = null
        if (content.id) {
          uuid = this.contentID(content.id)
        } else {
          uuid = this.mintID()
        }
        const local = {
          '@context': {
            '@vocab': AS2
          },
          id: this.url(`/content/${uuid}`),
          'published': (new Date()).toISOString(),
          'updated': (new Date()).toISOString()
        }
        const def = _.extend(local, content)
        promises.push(new Promise((resolve, reject) => {
          as2.import(def, (err, imported) => {
            if (err) {
              reject(err)
            } else {
              this.content.set(uuid, imported)
              resolve()
            }
          })
        }))
      }
    }
    return Promise.all(promises)
  }

  contentID (id) {
    const match = id.match(`${this.urlRoot}/content/(.*)$`)
    if (!match) {
      throw new Error(`${id} does not look like a content ID for this server`)
    } else {
      return match[1]
    }
  }

  async getPublicKey (keyId) {
    debug(`fetching ${keyId}`)
    const res = await fetch(this.fixupRemote(keyId))
    debug(`got response code ${res.status}`)
    debug(`got response headers ${res.headers}`)
    const json = await res.json()
    debug(json)
    // FIXME: namespaces yo
    const publicKey = json['https://w3id.org/security/v1publicKey']
    if (!publicKey) {
      throw new Error(`No public key found for ${keyId}`)
    }
    const pem = publicKey['https://w3id.org/security/v1publicKeyPem']
    if (!pem) {
      throw new Error(`No public key PEM found for ${keyId}`)
    }
    return pem
  }
}

module.exports = ActivityPubMock

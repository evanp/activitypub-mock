// batch-utils.js -- Useful batch or test utils
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:batch-utils')
const _ = require('lodash')

const props = require('./props')

const bt = (prop, type, value) => {
  return (err, body) => {
    debug(prop)
    debug(_.get(body, prop))
    assert.ifError(err)
    assert.isObject(body)
    assert.ok(_.has(body, prop), `Argument does not have ${prop}`)
    assert.typeOf(_.get(body, prop), type)
    if (!_.isUndefined(value)) {
      assert.equal(_.get(body, prop), value)
    }
  }
}

const mtb = (url, type, key) => {
  return {
    async topic () {
      const headers = {Accept: type}
      if (key) {
        headers.Authorization = `Bearer ${key}`
      }
      return fetch(url, {headers: headers})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
    },
    'it has the right media type': (err, res) => {
      assert.ifError(err)
      assert.equal(res.headers.get('content-type'), type)
    }
  }
}

const id2url = (id) => {
  return id.replace(props.urlRoot, `http://${props.address}:${props.port}`)
}

module.exports = {
  bt: bt,
  mtb: mtb,
  id2url: id2url
}

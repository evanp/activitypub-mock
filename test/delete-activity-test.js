// delete-activity-test.js -- Delete activity
//
// Copyright 2018 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:delete-activity-test')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')
const {bt, id2url} = require('./batch-utils')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user3/outbox`

vows.describe('Delete activity')
  .addBatch(mockBatch({
    'and we post an Delete activity': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user3}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Delete',
            summary: 'user3 deleted a note',
            to: 'https://www.w3.org/ns/activitystreams#Public',
            object: props.content[1].id
          })
        })
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 201)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
        assert.isString(res.headers.get('location'))
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has an ID': bt('id', 'string'),
        'it has an object id': bt('object', 'string'),
        'and we GET the deleted object': {
          async topic (activity) {
            const id = activity.object
            const url = id2url(id)
            return fetch(url, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user3}`}})
          },
          'it fails correctly': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
            assert.equal(res.status, 410)
          }
        }
      }
    }
  }))
  .export(module)

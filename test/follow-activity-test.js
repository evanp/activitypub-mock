// follow-activity-test.js -- Follow activity
//
// Copyright 2018 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:follow-activity-test')
const _ = require('lodash')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')
const {bt} = require('./batch-utils')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user4/outbox`
const FOLLOWING1 = `http://${props.address}:${props.port}/user/user4/following`
const FOLLOWERS1 = `http://${props.address}:${props.port}/user/user5/followers`
const USER4 = 'https://activitypub.test/user/user4'
const USER5 = 'https://activitypub.test/user/user5'

vows.describe('Follow activity')
  .addBatch(mockBatch({
    'and we post an Follow activity': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user4}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Follow',
            summary: 'user4 followed user5',
            to: 'https://www.w3.org/ns/activitystreams#Public',
            object: 'https://activitypub.test/user/user5'
          })
        })
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 201)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
        assert.isString(res.headers.get('location'))
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has an ID': bt('id', 'string'),
        'and we GET the actor\'s following collection': {
          async topic (activity) {
            return fetch(FOLLOWING1, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user4}`}})
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
            assert.equal(res.status, 200)
          },
          'and we review the body': {
            async topic (res) {
              return res.json()
            },
            'it works': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
            },
            'it has the object in it': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
              assert.isObject(json.first)
              if (_.isObject(json.first.items)) {
                assert.equal(json.first.items.id, USER5)
              } else if (_.isArray(json.first.items)) {
                assert.ok(_.some(json.first.items, (item) => { return item.id === USER5 }))
              }
            }
          }
        },
        'and we GET the object\'s followers collection': {
          async topic (activity) {
            return fetch(FOLLOWERS1, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user4}`}})
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
            assert.equal(res.status, 200)
          },
          'and we review the body': {
            async topic (res) {
              return res.json()
            },
            'it works': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
            },
            'it has the actor in it': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
              assert.isObject(json.first)
              if (_.isObject(json.first.items)) {
                assert.equal(json.first.items.id, USER4)
              } else if (_.isArray(json.first.items)) {
                assert.ok(_.some(json.first.items, (item) => { return item.id === USER4 }))
              }
            }
          }
        }
      }
    }
  }))
  .export(module)

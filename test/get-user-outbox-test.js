// get-user-outbox-test.js -- GET /user/:id/outbox
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:get-user-outbox-test')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {AS2_TYPE, JSONLD_TYPE, JSON_TYPE} = require('./media-types')
const {bt, mtb} = require('./batch-utils')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user1/outbox`
const OUTBOX2 = `http://${props.address}:${props.port}/user/user2/outbox`

vows.describe('GET /user/:id/outbox')
  .addBatch(mockBatch({
    'and we get a non-empty outbox': {
      async topic () {
        const url = OUTBOX1
        return fetch(url, {headers: {Accept: JSONLD_TYPE}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
        debug(res.headers)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has the correct ID': bt('id', 'string', `${props.urlRoot}/user/user1/outbox`),
        'it has the correct type': bt('type', 'string', 'Collection'),
        'it has the correct summary': bt('summary', 'string', 'user1 outbox'),
        'it has the correct totalItems': bt('totalItems', 'number', props.outboxes.user1.length),
        'it has a first property': bt('first', 'object'),
        'and we review the first page': {
          topic (body) {
            return body.first
          },
          'it has the correct ID': bt('id', 'string', `${props.urlRoot}/user/user1/outbox/page/1`),
          'it has the correct type': bt('type', 'string', 'CollectionPage'),
          'it has the correct summary': bt('summary', 'string', 'page 1 of user1 outbox'),
          'the items property looks correct': (err, first) => {
            assert.ifError(err)
            assert.isObject(first)
            assert.include(first, 'items')
            assert.isArray(first.items)
            assert.lengthOf(first.items, props.outboxes.user1.length)
            debug(first.items)
            for (const item of first.items) {
              assert.isObject(item)
              assert.include(item, 'type')
              assert.isString(item.type)
              assert.include(item, 'id')
              assert.isString(item.id)
            }
          }
        }
      }
    },
    'and we get an outbox with the AS2 media type': mtb(OUTBOX1, AS2_TYPE),
    'and we get an outbox with the JSON media type': mtb(OUTBOX1, JSON_TYPE),
    'and we get an empty outbox': {
      async topic () {
        const url = OUTBOX2
        return fetch(url, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user2}`}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
        debug(res.headers)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has the correct ID': bt('id', 'string', `${props.urlRoot}/user/user2/outbox`),
        'it has the correct type': bt('type', 'string', 'Collection'),
        'it has the correct summary': bt('summary', 'string', 'user2 outbox'),
        'it has the correct totalItems': bt('totalItems', 'number', 0)
      }
    }
  }))
  .export(module)

// get-user-publickey-test.js -- GET /user/:id/publickey
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:get-user-publickey-test')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {AS2_TYPE, JSONLD_TYPE, JSON_TYPE} = require('./media-types')
const {bt, mtb} = require('./batch-utils')

const PUBLICKEY1 = `http://${props.address}:${props.port}/user/user1/publickey`

const SEC = 'https://w3id.org/security/v1'

vows.describe('GET /user/:id/publickey')
  .addBatch(mockBatch({
    'and we get a publickey': {
      async topic () {
        return fetch(PUBLICKEY1, {headers: {Accept: JSONLD_TYPE}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 200)
        debug(res.headers)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has the correct ID': bt('id', 'string', `${props.urlRoot}/user/user1/publickey`),
        'it has the correct type': bt('type', 'string', `${SEC}Key`),
        'it has the correct owner': bt(`${SEC}owner`, 'string', `${props.urlRoot}/user/user1`),
        'it has the correct summary': bt('summary', 'string', 'public key for user1'),
        'it has the correct publicKeyPem': bt(`${SEC}publicKeyPem`, 'string', props.rsa.user1.public)
      }
    },
    'and we get an publickey with the AS2 media type': mtb(PUBLICKEY1, AS2_TYPE),
    'and we get an publickey with the JSON media type': mtb(PUBLICKEY1, JSON_TYPE)
  }))
  .export(module)

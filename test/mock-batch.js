// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const _ = require('lodash')
const vows = require('perjury')
const {assert} = vows

const ActivityPubMock = require('../index')
const props = require('./props')
const remoteProps = require('./remote-props')

const mockBatch = (tests = {}) => {
  const base = {
    'When we start ActivityPubMock instances': {
      async topic () {
        const mock = new ActivityPubMock(props)
        const remote = new ActivityPubMock(remoteProps)
        await Promise.all([mock.start(), remote.start()])
        return [mock, remote]
      },
      'it works': (err, mocks) => {
        assert.ifError(err)
        assert.isArray(mocks)
        assert.lengthOf(mocks, 2)
        const [mock, remote] = mocks
        assert.isObject(mock)
        assert.isObject(remote)
      },
      async teardown (mocks) {
        const [mock, remote] = mocks
        return Promise.all([mock.stop(), remote.stop()])
      }
    }
  }
  _.extend(base['When we start ActivityPubMock instances'], tests)
  return base
}

module.exports = mockBatch

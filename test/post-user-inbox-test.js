// post-user-inbox-test.js -- POST /user/:id/inbox
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const vows = require('perjury')
const {assert} = vows
const httpSignature = require('http-signature')
const fetch = require('node-fetch')

const props = require('./props')
const remoteProps = require('./remote-props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')

const ACTIVITYID = `http://${remoteProps.address}:${remoteProps.port}/activity/37be562a-eb1e-11e7-9c1e-c8f73398600c`
const ACTORID = `http://${remoteProps.address}:${remoteProps.port}/user/remote5`
const ACTIVITY = {
  id: ACTIVITYID,
  actor: ACTORID,
  type: 'Listen',
  summary: 'User Three listened to Promises, Promises',
  to: 'https://www.w3.org/ns/activitystreams#Public',
  object: {
    type: 'Audio',
    id: 'https://musicbrainz.org/recording/8d152c22-d5ed-48ab-8903-f6f379690bea',
    name: 'Promises, Promises'
  }
}

vows.describe('POST /user/:id/inbox')
  .addBatch(mockBatch({
    'and we post to an inbox': {
      async topic () {
        return new Promise((resolve, reject) => {
          const body = JSON.stringify(ACTIVITY)
          const options = {
            host: props.address,
            port: props.port,
            path: '/user/user1/inbox',
            method: 'POST',
            headers: {
              Accept: JSONLD_TYPE,
              'Content-Type': JSONLD_TYPE,
              'Content-Length': Buffer.byteLength(body, 'utf8')
            }
          }
          const req = http.request(options, (res) => {
            resolve(res)
          })
          req.on('error', reject)
          httpSignature.sign(req, {
            key: remoteProps.rsa.remote5.private,
            keyId: ACTORID
          })
          req.write(body)
          req.end()
        })
      },
      'it gives a 202 Accepted response': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.statusCode, 202)
      },
      'and we wait a second': {
        async topic () {
          return new Promise((resolve, reject) => {
            setTimeout(resolve, 1000)
          })
        },
        'it works': (err) => {
          assert.ifError(err)
        },
        'and we get the user inbox': {
          async topic () {
            const INBOX1 = `http://${props.address}:${props.port}/user/user1/inbox`
            return fetch(INBOX1, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user1}`}})
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
          },
          'and we check the body': {
            topic (res) {
              return res.json()
            },
            'it works': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
            },
            'it includes the posted activity': (err, json) => {
              assert.ifError(err)
              assert.isObject(json)
              assert.isObject(json.first)
              assert.isArray(json.first.items)
              assert.ok(json.first.items.find((item) => { return item.id === ACTIVITYID }))
            }
          }
        }
      }
    }
  }))
  .export(module)

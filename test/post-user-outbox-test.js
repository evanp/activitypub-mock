// post-user-outbox-test.js -- POST /user/:id/outbox
//
// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:post-user-outbox-test')
const _ = require('lodash')

const props = require('./props')
const mockBatch = require('./mock-batch')

const {JSONLD_TYPE} = require('./media-types')
const {bt, id2url} = require('./batch-utils')

const OUTBOX1 = `http://${props.address}:${props.port}/user/user1/outbox`

vows.describe('POST /user/:id/outbox')
  .addBatch(mockBatch({
    'and we post to an outbox': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user1}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Listen',
            summary: 'User1 listened to The Promise',
            to: 'https://www.w3.org/ns/activitystreams#Public',
            object: {
              type: 'Audio',
              id: 'https://musicbrainz.org/recording/d9f96478-a818-4382-a777-b455877f30dc',
              name: 'The Promise'
            }
          })
        })
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 201)
        assert.equal(res.headers.get('content-type'), JSONLD_TYPE)
        assert.isString(res.headers.get('location'))
      },
      'and we review the body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, body) => {
          assert.ifError(err)
          assert.isObject(body)
          debug(body)
        },
        'it has an ID': bt('id', 'string'),
        'it has the correct type': bt('type', 'string', 'Listen'),
        'it has the correct summary': bt('summary', 'string', 'User1 listened to The Promise'),
        'it has an object': bt('object', 'object')
      },
      'and we get the activity': {
        topic (posted) {
          const location = posted.headers.get('location')
          const url = id2url(location)
          return fetch(url, {headers: {Accept: JSONLD_TYPE, Authorization: `Bearer ${props.oauth2.user1}`}})
        },
        'it works': (err, res) => {
          assert.ifError(err)
          assert.isObject(res)
          assert.equal(res.status, 200)
        },
        'and we examine the body': {
          topic (res) {
            return res.json()
          },
          'it works': (err, body) => {
            assert.ifError(err)
            assert.isObject(body)
          },
          'it has an ID': bt('id', 'string'),
          'it has the correct type': bt('type', 'string', 'Listen'),
          'it has the correct summary': bt('summary', 'string', 'User1 listened to The Promise'),
          'it has an object': bt('object', 'object')
        }
      },
      'and we get the outbox': {
        async topic () {
          const res = await fetch(OUTBOX1)
          return res.json()
        },
        'it works': (err, outbox) => {
          assert.ifError(err)
          assert.isObject(outbox)
        },
        'and we look for the new activity': {
          topic (outbox, posted) {
            const id = posted.headers.get('location')
            return _.find(outbox.first.items, _.matchesProperty('id', id))
          },
          'it works': (err, activity) => {
            assert.ifError(err)
            assert.isObject(activity)
          }
        }
      }
    },
    'and we post an outbox with another user\'s key': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            Authorization: `Bearer ${props.oauth2.user2}`,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Listen',
            summary: 'User1 listened to The Promise',
            object: {
              type: 'Audio',
              id: 'https://musicbrainz.org/recording/d9f96478-a818-4382-a777-b455877f30dc',
              name: 'The Promise'
            }
          })
        })
      },
      'it fails correctly': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 403)
      }
    },
    'and we post an outbox with no key': {
      async topic () {
        return fetch(OUTBOX1, {
          method: 'POST',
          headers: {
            Accept: JSONLD_TYPE,
            'Content-Type': JSONLD_TYPE
          },
          body: JSON.stringify({
            type: 'Listen',
            summary: 'User1 listened to The Promise',
            object: {
              type: 'Audio',
              id: 'https://musicbrainz.org/recording/d9f96478-a818-4382-a777-b455877f30dc',
              name: 'The Promise'
            }
          })
        })
      },
      'it fails correctly': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
        assert.equal(res.status, 403)
      }
    }
  }))
  .export(module)

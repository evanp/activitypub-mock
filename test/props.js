// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

module.exports = {
  port: 8080,
  address: '127.0.0.1',
  urlRoot: 'https://activitypub.test',
  users: {
    'user0': {
      type: 'Person',
      name: 'User Zero'
    },
    'user1': {
      type: 'Person',
      name: 'User One'
    },
    'user2': {
      type: 'Person',
      name: 'User Two'
    },
    'user3': {
      type: 'Person',
      name: 'User Three'
    },
    'user4': {
      type: 'Person',
      name: 'User Four'
    },
    'user5': {
      type: 'Person',
      name: 'User Five'
    }
  },
  oauth2: {
    'user0': '4dd91f3e-88f0-40a6-88d7-a8d213f75dd4',
    'user1': '9fe2f042-643b-4e90-8f46-c0817bdfdaf6',
    'user2': '4b10bbdd-e313-49d7-9aaa-18aebd467172',
    'user3': 'dd053887-3c2b-4028-a223-06c920f42694',
    'user4': '4655d4e3-412d-475d-a564-21e61d02ccbb',
    'user5': 'fb249fce-2aab-42bd-b744-b48d4eb2b421'
  },
  inboxes: {
    'user1': [
      {
        id: 'https://remote.test/create/2',
        type: 'Create',
        actor: 'https://remote.test/user/remote4',
        summary: 'remote4 replied to a note',
        object: {
          id: 'https://remote.test/note/2',
          type: 'Note',
          content: 'I bet it does.',
          inReplyTo: 'https://remote.test/note/1'
        }
      },
      {
        id: 'https://remote.test/like/1',
        type: 'Like',
        actor: 'https://remote.test/user/remote4',
        summary: 'remote4 liked a note',
        object: 'https://remote.test/note/1'
      },
      {
        id: 'https://remote.test/create/1',
        type: 'Create',
        actor: 'https://remote.test/user/remote5',
        summary: 'remote5 created a note',
        object: {
          id: 'https://remote.test/note/1',
          type: 'Note',
          content: 'My dog has fleas.'
        }
      }
    ]
  },
  outboxes: {
    'user1': [
      {
        id: 'https://activitypub.test/activity/1',
        type: 'Create',
        summary: 'user1 created a note',
        object: {
          id: 'https://activitypub.test/note/1',
          type: 'Note',
          content: 'What hath God wrought.'
        }
      },
      {
        id: 'https://activitypub.test/activity/2',
        type: 'Create',
        summary: 'user1 created a note',
        object: {
          id: 'https://activitypub.test/note/2',
          type: 'Note',
          content: 'Mr. Watson — Come here — I want to see you.'
        }
      }
    ]
  },
  followers: {
    user1: [
      'https://activitypub.test/user/user2',
      'https://activitypub.test/user/user3'
    ],
    user0: [
      'https://remote.test/user/remote5',
      'https://remote.test/user/remote4'
    ],
    user3: [
      'https://activitypub.test/user/user2',
      'https://remote.test/user/remote4'
    ]
  },
  following: {
    user1: [
      'https://activitypub.test/user/user2',
      'https://remote.test/user/remote5'
    ]
  },
  liked: {
    user1: [
      {
        id: 'https://activitypub.test/note/1',
        type: 'Note'
      },
      {
        id: 'https://activitypub.test/image/3',
        type: 'Image'
      },
      {
        id: 'https://remote.test/video/4',
        type: 'Video'
      },
      {
        id: 'https://remote.test/note/2',
        type: 'Note'
      }
    ]
  },
  rsa: {
    user1: {
      public: '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAg2XDIhLgVEG068xkiMVfHIKlQM8/XPIf5C9KJ676USVSnNQV0+XWZpMf3UNA\nA4M908kczDPzIBXNdIK96c27v0j8zIowy7BpzF5bIEDP0F4FnO/H0qahVgUanuaMEs0EONyT\nteHsX6bwyhWWxUdT4CZgO3j2O6PFGlOma1iRobpkdd2a3R3s21C3Tj/sZ/MieE12UWu/DLmj\nl+2iAjIy2sj40EhsMHJZbLESOCMG0ghr/FcvDk3dy0bnYBsaU+L8bP7Dgec5YjlOH5wUJJfH\nXxUt8t8FtYu5t/LV5J2o+Ml7glkXHq6YCOOOvtDo+xIV5/PmHlY4x0HELIYoRGBtLwIDAQAB\n-----END RSA PUBLIC KEY-----\n',
      private: '-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAg2XDIhLgVEG068xkiMVfHIKlQM8/XPIf5C9KJ676USVSnNQV0+XWZpMf\n3UNAA4M908kczDPzIBXNdIK96c27v0j8zIowy7BpzF5bIEDP0F4FnO/H0qahVgUanuaMEs0E\nONyTteHsX6bwyhWWxUdT4CZgO3j2O6PFGlOma1iRobpkdd2a3R3s21C3Tj/sZ/MieE12UWu/\nDLmjl+2iAjIy2sj40EhsMHJZbLESOCMG0ghr/FcvDk3dy0bnYBsaU+L8bP7Dgec5YjlOH5wU\nJJfHXxUt8t8FtYu5t/LV5J2o+Ml7glkXHq6YCOOOvtDo+xIV5/PmHlY4x0HELIYoRGBtLwID\nAQABAoIBAHhIOnXBo9tdftnv9AQD8YouyknmrhFOkAUZiR+EnR6VNOfdedHM7xRdD+lDe6D7\njiiGTLddwu0XKEnEuUcDDyAPF2PCCOMdLexoX7DJw7Lxrt+33EsoQD53U6QsVgOL65aFzS3N\nj80MDBZh5u/W2KAzYRpwI6/6vh7Yflp77GyH06W2Zdt9mnzmxPPv1NGzAdQHZ7Ef5LK+M2Ks\nWOLXGQLbtaaKQT7bpJnFcrAzPUhueeqOSM0ZMpc6Fu+YqLqer76nzvZbJ5UHLFSxHd6ZGuPy\nYxFN5d/lY2MzMEQK0AICklCfsY7k1jwB67WFobzpiJsxW0rLLLr710doiO02ANECgYEA6P9r\nvGsXXIUOa8doGmH9ksmcJFM23ahDlPkGRQOuAGDCtQv7rRXlZNv+6V6idMQPbY0/c4rxiEyu\nabcpLCYUfV1rzcc7cAtqDJ/xqZ7WqO06EgKNKo/df5fUYcbf4288y2WIJ+uBS9FQqvWVrldl\nl8h5TQZd2nqjg8TP+WI+M7kCgYEAkF6WPnuRvvqq3eVf3OgTtw4seeJrqb3au1WxcxNzgBnQ\nXBasbP+sFivwfyIYdUuYhTU7vdZMG3WSbTClmRPi1zKZ0Z++puGHEir6RhCmkzhBq52RGDuG\n/2/HuuHAz0Rm5puRZlhdFLTJ7uMoPktC4eULZMX2Om/3BTFg7qr47CcCgYBpBVgmYY7yIPdT\nFVqTKcAGPeGnbeq5FnL/3w84ZGQ1v2eD+5bBebEgORk5T3Kidw0NMDF9fw9HhGjJNcnVIITz\nzPwekML1ye9PKXjgH7xaADPAkm3W1WndbS5JtD6OYUFh3K32cGyjJAHcTT3ei347YB1s3oXH\nSKgTaM7ldabb4QKBgGmBpr19IxN2L99AnWFc/eN+6UQeE96WED9uLePSqEtLEjnqnBKAeyws\n6nKO4V7/YzSepc1jynUa5GsF7U/E97eblExdOUDOex7s58DCtGnM6DlaSspx3oPZFhdemsEe\n1TDdKlKoa0YRpa3+ts9nwzZBZKU+A2fLobRa1OM3cflrAoGAKNoLRGVANV9X/zyIUv7Aq+rp\nSMCWh0WMrKDioKeXtnSIgXcGz7Zy6PxoCsuXYuNpnGaGLfXd/52wa/hU1Ehbs2Gncjjh3uz1\nSfi1z4dmAPsZjIzZdGCXWEYlMJSDvMiHBzL4Rq+bRAmxNLQ1akXnLDcg5MLedeRGe1OaCuto\npeo=\n-----END RSA PRIVATE KEY-----\n'
    }
  },
  rootMap: {
    'https://remote.test': 'http://localhost:8081',
    'https://activitypub.test': 'http://localhost:8080'
  },
  content: [
    {
      id: 'https://activitypub.test/content/4b8d06b0-7a45-4f92-9fe4-a5d905711014',
      type: 'Note',
      attributedTo: 'https://activitypub.test/user/user3',
      summary: 'A note by user3',
      content: 'To serve man'
    },
    {
      id: 'https://activitypub.test/content/84878983-8da4-4679-bd56-f3c70e02b29a',
      type: 'Note',
      attributedTo: 'https://activitypub.test/user/user3',
      summary: 'A note by user3',
      content: 'Able was I ere I saw Elba.'
    }
  ]
}

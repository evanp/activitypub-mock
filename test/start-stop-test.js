// Copyright 2017 Evan Prodromou <evan@prodromou.name>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('activitypub-mock:start-stop-test')
const ActivityPubMock = require('../index')
const props = require('./props')

vows.describe('start and stop a mock server')
  .addBatch({
    'When we create an ActivityPubMock': {
      topic () {
        return new ActivityPubMock(props)
      },
      'it works': (err, mock) => {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'and we start it': {
        async topic (mock) {
          return mock.start()
        },
        'it works': (err) => {
          debug(err)
          assert.ifError(err)
        },
        'and we fetch the version': {
          async topic () {
            return fetch(`http://${props.address}:${props.port}/version`)
          },
          'it works': (err, res) => {
            assert.ifError(err)
            assert.isObject(res)
            assert.equal(res.status, 200)
          },
          'and we stop it': {
            async topic (res, ignore, mock) {
              return mock.stop()
            },
            'it works': (err) => {
              assert.ifError(err)
            }
          }
        }
      }
    }
  })
  .export(module)
